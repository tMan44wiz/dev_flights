let currentPage = 1;
let itemsPerPage = 10;
let searchWord = '';
let flights = [];


const validateParameters = (page, size, searchWord) => {
    const isValidPage = Number.isInteger(page) && page > 0;
    const isValidSize = Number.isInteger(size) && size > 0 && size <= 100;
    const isValidSearch = typeof searchWord === 'string';
    return isValidPage && isValidSize && isValidSearch;
};

const fetchFlights = async () => {
    if (!validateParameters(currentPage, itemsPerPage, searchWord)) {
        window.location.href = '/bad-request.html';
        return;
    }
    let url = `http://localhost:3000/flights?page=${currentPage}&size=${itemsPerPage}`;
    try {
        if (searchWord) {
            url += `&code=${encodeURIComponent(searchWord)}`;
        }
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        const flightResponse = await response.json();
        flights = flightResponse.resources;
        renderTable(flights);
        console.log("FLIGHTS::: ", flights);
    } catch (error) {
        console.error('Failed to fetch flights:', error);
        // document.getElementById('app').innerHTML = 'Failed to load flight data.';
    }
}

const renderTable = (flights) => {
    const tableBody = document.getElementById('flight-table-body');
    tableBody.innerHTML = ''; // Clear existing table data

    flights.forEach((flight, index) => {
        const tableRow = document.createElement("tr");
        tableRow.className = index % 2 === 0 ? "bg-gray-100" : "bg-white";

        const codeData = document.createElement("td");
        codeData.className = 'border px-6 py-4';
        codeData.textContent = flight.code;
        tableRow.appendChild(codeData);

        const capacityData = document.createElement("td");
        capacityData.className = 'border px-6 py-4';
        capacityData.textContent = flight.capacity;
        tableRow.appendChild(capacityData);

        const departureDateData = document.createElement("td");
        departureDateData.className = 'border px-6 py-4';
        departureDateData.textContent = new Date(flight.departureDate).toLocaleDateString();
        tableRow.appendChild(departureDateData);

        // Append the table row ton the table body
        tableBody.appendChild(tableRow);
    });
};

const updatePage = (newPage) => {
    currentPage = newPage;
    fetchFlights();
};

const updateItemsPerPage = (newItemsPerPage) => {
    itemsPerPage = newItemsPerPage;
    currentPage = 1; // Reset to first page when changing items per page
    fetchFlights();
};

const updateSearchWord = (newSearchWord) => {
    searchWord = newSearchWord;
    currentPage = 1; // Reset to first page when changing the search query
    fetchFlights();
};

document.getElementById('prevPageBtn').addEventListener('click', () => {
    if (currentPage > 1) {
        updatePage(currentPage - 1);
    }
});

document.getElementById('nextPageBtn').addEventListener('click', () => {
    updatePage(currentPage + 1);
});

document.getElementById('perPageSelect').addEventListener('change', (event) => {
    updateItemsPerPage(parseInt(event.target.value));
});

document.getElementById('searchInput').addEventListener('input', (event) => {
    updateSearchWord(event.target.value);
});


// Fetch and render the table on page load
fetchFlights();