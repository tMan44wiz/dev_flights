import { createBrowserRouter } from "react-router-dom";
import HomePage from "../pages/home_page";
import ErrorPage from "../pages/error_page";

const routes = createBrowserRouter([
    {
        path: "",
        element: <HomePage />,
        errorElement: <ErrorPage />,
    },
    {
        path: "/error_page",
        element: <HomePage />,
        errorElement: <ErrorPage />,
    }
]);

export default routes;