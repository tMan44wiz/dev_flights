import React, { useState, useEffect } from 'react';
import { Helmet, HelmetProvider } from "react-helmet-async";
import axios from "axios";
import { Dialog } from '@headlessui/react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const HomePage = () => {

    const [state, setState] = useState({
        currentPage: 1,
        itemsPerPage: 10,
        searchWord: "",
        isOpen: false,
        allFlights: [],
        newFlight: { code: '', capacity: 0, departureDate: '' },
    });

    const validateParameters = (page, size, search) => {
        const isValidPage = Number.isInteger(page) && page > 0;
        const isValidSize = Number.isInteger(size) && size > 0 && size <= 100;
        const isValidSearch = typeof search === 'string';
        return isValidPage && isValidSize && isValidSearch;
    };

    const fetchFlights = async () => {

        if (!validateParameters(state.currentPage, state.itemsPerPage, state.searchWord)) {
            window.location.href = "/error_page";
            return;
        }

        try {
            let url = `http://localhost:3000/flights?page=${state.currentPage}&size=${state.itemsPerPage}`;
            if (state.searchWord) {
                url += `&code=${encodeURIComponent(state.searchWord)}`;
            }
            const response = await axios.get(url);
            setState(prevState => ({
                ...prevState,
                allFlights: response.data.resources,
            }));
        } catch (error) {
            toast.error("Failed to load flight data.");
        }
    };

    useEffect(() => {
        fetchFlights();
    }, [state.currentPage, state.itemsPerPage, state.searchWord]);

    const handlePageChange = (newPage) => {
        if (validateParameters(newPage, state.itemsPerPage, state.searchWord)) {
            setState(prevState => ({
                ...prevState,
                currentPage: newPage,
            }));
        } else {
            toast.error("Invalid page number.");
        }
    };

    const handleItemsPerPageChange = (event) => {
        const newItemsPerPage = parseInt(event.target.value);
        if (validateParameters(state.currentPage, newItemsPerPage, state.searchWord)) {
            setState(prevState => ({
                ...prevState,
                itemsPerPage: newItemsPerPage,
                currentPage: 1, // Reset to first page when changing items per page
            }));
        } else {
            toast.error("Invalid items per page value.");
        }
    };

    const handleSearchChange = (event) => {
        setState(prevState => ({
            ...prevState,
            searchWord: event.target.value,
            currentPage: 1, // Reset to first page when changing the search query
        }));
    };

    const handleCreateFlight = async () => {
        // Check if flight code is unique
        const isCodeUnique = state.allFlights.every(flight => flight.code !== state.newFlight.code);
        if (!isCodeUnique) {
            toast.error("Flight code must be unique.");
            return;
        }

        // Send request to create a new flight
        try {
            const response = await axios.post('http://localhost:3000/flights', state.newFlight);
            setState(prevState => ({
                ...prevState,
                allFlights: [...state.allFlights, response.data],
                newFlight: { code: '', capacity: 0, departureDate: '' },
                isOpen: false,
            }));
            toast.success("Flight created successfully.");
        } catch (error) {
            toast.error("Failed to create flight.")
        }
    };

    const metaData = {
        title: "CBTAS - Home",
        meta: [
            {
                name: "description",
                content: "Central Billing and Tax Administration System (CBTAS)."
            },
            {
                name: "keywords",
                content: "CBTAS, Central Billing System, Tax Clearance Certificates, TCC,"
            },
            {
                name: "author",
                content: "4Core Integrated Services"
            }
        ],
    };

    return (
        <main>
            <HelmetProvider>
                <Helmet {...metaData} />
            </HelmetProvider>

            <div className="container mx-auto p-5">
                <h1 className="text-3xl font-bold text-center">Flight List</h1>

                <div className="mt-10 flex justify-between items-center">

                    {/*==== Search field ====*/}
                    <input id="searchInput" type="text"
                           value={state.searchWord}
                           onChange={handleSearchChange}
                           placeholder="Search by flight code"
                           className="px-2 py-1 border rounded"
                    />

                    {/*==== Search field ====*/}
                    <button
                        onClick={() => {
                            setState(prevState => ({
                                ...prevState,
                                isOpen: true,
                            }));
                        }}
                        className="px-4 py-2 bg-green-500 text-white rounded"
                    >
                        Add Flight
                    </button>
                </div>

                {/*==== Flight Table ====*/}
                <table className="h-auto w-full mt-2">
                    <thead className="bg-gray-100 text-left">
                    <tr>
                        <th className="w-1/3 px-4 py-2 text-gray-800 bg-gray-200">Flight Code</th>
                        <th className="w-1/3 px-4 py-2 text-gray-800 bg-gray-200">Capacity</th>
                        <th className="w-1/3 px-4 py-2 text-gray-800 bg-gray-200">Departure Date</th>
                    </tr>
                    </thead>

                    <tbody>
                        {state.allFlights.map((flight, index) => (
                            <tr key={flight.code} className={index % 2 === 0 ? 'bg-gray-100' : 'bg-white'}>
                                <td className="border px-6 py-4">{flight.code}</td>
                                <td className="border px-6 py-4">{flight.capacity}</td>
                                <td className="border px-6 py-4">{new Date(flight.departureDate).toLocaleString()}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>

                {/*==== Navigation controller ====*/}
                <div className="mt-3 flex justify-between items-center mb-4">
                    <div>
                        <label htmlFor="perPageSelect" className="mr-2">Items Per Page:</label>
                        <select id="perPageSelect"
                                value={state.itemsPerPage}
                                onChange={handleItemsPerPageChange}
                                className="px-2 py-1 border rounded"
                        >
                            <option value="2">2</option>
                            <option value="10" selected>10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                        </select>
                    </div>
                    <div>
                        <button
                            onClick={() => handlePageChange(state.currentPage - 1)}
                            disabled={state.currentPage === 1}
                                className={`px-4 py-1.5 ${(state.currentPage > 1) ? "bg-gray-500" : "bg-gray-300"} text-white text-sm rounded mr-2`}>Previous
                        </button>
                        <button
                            onClick={() => handlePageChange(state.currentPage + 1)}
                                className="px-4 py-1.5 bg-gray-500 text-white text-sm rounded">Next
                        </button>
                    </div>
                </div>
            </div>

            {/* Modal for Creating New Flight */}
            <Dialog open={state.isOpen}
                    onClose={() => {
                        setState(prevState => ({
                            ...prevState,
                            isOpen: false,
                        }));
                    }}
                    className="fixed inset-0 z-10 flex items-center justify-center"
            >
                <div className="bg-white rounded-lg p-6 w-96 shadow-lg">
                    <Dialog.Title className="text-xl font-semibold">Add New Flight</Dialog.Title>
                    <div className="mt-4">
                        <label className="block">Flight Code</label>
                        <input
                            type="text"
                            className="w-full px-2 py-1 border rounded"
                            value={state.newFlight.code}
                            onChange={(e) => {
                                setState(prevState => ({
                                    ...prevState,
                                    newFlight: { ...state.newFlight, code: e.target.value },
                                }));
                            }}
                        />
                    </div>
                    <div className="mt-4">
                        <label className="block">Capacity</label>
                        <input
                            type="number"
                            className="w-full px-2 py-1 border rounded"
                            value={state.newFlight.capacity}
                            onChange={(e) => {
                                setState(prevState => ({
                                    ...prevState,
                                    newFlight: { ...state.newFlight, capacity: parseInt(e.target.value) },
                                }));
                            }}
                        />
                    </div>
                    <div className="mt-4">
                        <label className="block">Departure Date</label>
                        <input
                            type="date"
                            className="w-full px-2 py-1 border rounded"
                            value={state.newFlight.departureDate}
                            onChange={(e) => {
                                setState(prevState => ({
                                    ...prevState,
                                    newFlight: { ...state.newFlight, departureDate: e.target.value },
                                }));
                            }}
                        />
                    </div>
                    {state.formError && <div className="text-red-600 mt-2">{state.formError}</div>}
                    <div className="mt-6 flex justify-end">
                        <button
                            onClick={() => {
                                setState(prevState => ({
                                    ...prevState,
                                    isOpen: false,
                                }));
                            }}
                            className="px-4 py-2 bg-gray-500 text-white rounded mr-2"
                        >
                            Cancel
                        </button>
                        <button
                            onClick={handleCreateFlight}
                            className="px-4 py-2 bg-blue-500 text-white rounded"
                        >
                            Create
                        </button>
                    </div>
                </div>
            </Dialog>

            <ToastContainer />
        </main>
    );
};

export default HomePage;
